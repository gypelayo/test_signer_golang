# Test Signer
Implemented with a Repository design pattern.

## Environment Setup
Requires a running **PostgreSQL** database running on the host. 
Environment configuration to emulate a terraform setup or similar
``` bash
export DB_CONNECTION="postgres://postgres:new_password@localhost/postgres?sslmode=disable"
export APP_PORT=8075
```
## Running
```
go run cmd/main.go
```
## Database tables
```
                                Table "public.tests"
  Column   |  Type   | Collation | Nullable |                Default                 
-----------+---------+-----------+----------+----------------------------------------
 test_id   | integer |           | not null | nextval('tests_test_id_seq'::regclass)
 user_id   | integer |           | not null | 
 questions | text[]  |           |          | 
 answers   | text[]  |           |          | 
```
```
                                          Table "public.users"
    Column     |          Type          | Collation | Nullable |                Default                 
---------------+------------------------+-----------+----------+----------------------------------------
 user_id       | integer                |           | not null | nextval('users_user_id_seq'::regclass)
 username      | character varying(255) |           | not null | 
 password_hash | character varying(255) |           | not null | 
```
```
                                              Table "public.signatures"
    Column    |            Type             | Collation | Nullable |                     Default                      
--------------+-----------------------------+-----------+----------+--------------------------------------------------
 signature_id | integer                     |           | not null | nextval('signatures_signature_id_seq'::regclass)
 test_id      | integer                     |           | not null | 
 signature    | text                        |           | not null | 
 timestamp    | timestamp without time zone |           |          | CURRENT_TIMESTAMP
```

## Testing
With the unit tests provided the aim was not 100% coverage or extensive covering of edge cases but demonstrating how the solutions can be tested.
run with:
```bash
go test ./...
```

## Requests
A requests.http file is provided to test functionality and provide example requests. In order to use it it's recommended to use for example the REST Client extension for Visual Studio Code or other tools compatible with. As an alternative the same request can be performed with POSTMAN or Curl.  

### Considered possible edge cases:
 - User trying to submit more than once answers for the same test
 - User trying wrong or invalid tokens
 - Token validation on submit test

## Concurrency handling
This project is designed with concurrency at its core, ensuring efficient and safe operation under high load and concurrent access. Here's how I handle concurrency across different layers of the application:

#### Database Layer
Our project utilizes PostgreSQL, a powerful relational database that supports high levels of concurrency. Key features include:

**Transactions**: Ensures complex operations involving multiple steps are treated as a single atomic unit, maintaining data integrity.
Locks: Prevents data inconsistencies by restricting simultaneous access to data being modified.
**Isolation Levels**: Offers a balance between performance and consistency, ensuring transactions operate with a clear view of the database state.
On the database side concurrency is not possible has the same resources are not accessed by different users and can only be accessed for a given user once per test.

#### Application Layer
Built in Go, the application leverages Go's native concurrency model, characterized by goroutines and channels, to manage concurrent operations effectively:

##### HTTP Server with Gin
I use the Gin framework for routing and handling HTTP requests. Gin is designed to spawn a new goroutine for each incoming request, allowing our application to serve multiple users concurrently. This approach maximizes throughput and responsiveness.

##### Safe Database Access
Database operations are encapsulated within repository layers, interfacing with PostgreSQL. These operations are designed to be safe and efficient under concurrent access by multiple users, thanks to PostgreSQL's robust concurrency control mechanisms.

## REST
This api aimed to be as RESTful as it could by aligning with the following requirements:

- **Client-Server Architecture**: The application clearly separates the client (making HTTP requests) from the server (handling those requests), allowing each to evolve independently.

- **Stateless**: Each request to the API includes all necessary information to process it. For instance, the verification request includes both the username and signature. This means the server does not need to maintain any session state between requests, adhering to the stateless constraint.

- **Uniform Interface**: The API uses HTTP methods appropriately (POST for creating or submitting data), and the paths (/create-user, /sign-test, /verify-signature) are resource-oriented, which is indicative of a uniform interface. Using HTTP status codes to indicate success or failure of requests would also contribute to this principle.

## Future improvements
Future improvements to this solution would be:
- Increased code coverage
- Improving the questions and answers structure to be stored in its own tables in the database
- A docker-compose file containing the PostgreSQL database and signer service
