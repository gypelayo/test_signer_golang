package utils

import (
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
)

func TestCreateAndVerifyToken(t *testing.T) {
	username := "testUser"

	tokenString, err := CreateToken(username)
	assert.NoError(t, err, "Creating token should not produce an error")
	assert.NotEmpty(t, tokenString, "Token string should not be empty")

	err = VerifyToken(tokenString, username)
	assert.NoError(t, err, "Verifying token should succeed with the correct username")
}

func TestVerifyTokenWithIncorrectUsername(t *testing.T) {
	username := "testUser"
	incorrectUsername := "wrongUser"

	tokenString, err := CreateToken(username)
	assert.NoError(t, err, "Creating token should not produce an error")

	err = VerifyToken(tokenString, incorrectUsername)
	assert.Error(t, err, "Verifying token with incorrect username should fail")
}

func TestVerifyExpiredToken(t *testing.T) {
	username := "expiredUser"
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": username,
		"exp":      time.Now().Add(-time.Second).Unix(),
	})

	tokenString, err := token.SignedString(secretKey)
	assert.NoError(t, err, "Creating token should not produce an error")

	err = VerifyToken(tokenString, username)
	assert.Error(t, err, "Expired token should not be verified successfully")
}

func TestVerifyTokenInvalidSignature(t *testing.T) {
	username := "testUser"
	tokenString, _ := CreateToken(username)
	tokenString += "tampering"

	err := VerifyToken(tokenString, username)
	assert.Error(t, err, "Token with tampered signature should not be verified successfully")
}

func TestVerifyTokenWithoutUsernameClaim(t *testing.T) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp": time.Now().Add(time.Hour * 24).Unix(),
	})

	tokenString, err := token.SignedString(secretKey)
	assert.NoError(t, err, "Creating token should not produce an error")

	err = VerifyToken(tokenString, "anyUser")
	assert.Error(t, err, "Token without a username claim should not be verified successfully")
}
