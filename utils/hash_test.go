package utils

import (
	"crypto/sha256"
	"encoding/hex"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHashPassword(t *testing.T) {
	password := "secretPassword123"
	hashed, err := HashPassword(password)
	assert.NoError(t, err, "Hashing the password should not produce an error")
	assert.NotEqual(t, password, hashed, "Hashed password should be different from the plaintext password")
}

func TestCheckPasswordHash(t *testing.T) {
	password := "secretPassword123"
	hashed, _ := HashPassword(password)

	// Test with correct password
	isValid := CheckPasswordHash(password, hashed)
	assert.True(t, isValid, "Password should be valid")

	// Test with incorrect password
	isValid = CheckPasswordHash("wrongPassword", hashed)
	assert.False(t, isValid, "Password should be invalid")
}

func TestGenerateSignature(t *testing.T) {
	username := "testUser"
	questions := []string{"What is your favorite color?", "What is your pet's name?"}

	signature := GenerateSignature(username, questions)

	// Basic checks to ensure a signature is generated
	assert.NotEmpty(t, signature, "Signature should not be empty")

	// Checking if the signature changes with different inputs
	differentQuestions := append([]string(nil), questions...) // Make a copy of questions to avoid modifying the original slice
	differentSignature := GenerateSignature(username, append(differentQuestions, "Another question"))
	assert.NotEqual(t, signature, differentSignature, "Signature should be different with different input")

	// More meaningful check: Verify the hash directly
	expectedSignature := generateExpectedSignature(username, questions)
	assert.Equal(t, expectedSignature, signature, "Generated signature does not match expected signature")
}

// Helper function for tests to generate the expected signature.
// This mirrors the GenerateSignature logic for validation purposes.
func generateExpectedSignature(username string, questions []string) string {
	combined := username + strings.Join(questions, "")
	hasher := sha256.New()
	hasher.Write([]byte(combined))
	return hex.EncodeToString(hasher.Sum(nil))
}
