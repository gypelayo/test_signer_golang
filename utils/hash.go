// utils.go in /pkg/utils

package utils

import (
	"crypto/sha256"
	"encoding/hex"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

// HashPassword generates a bcrypt hash of the given password.
func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashedPassword), nil
}

// CheckPasswordHash compares a password with its bcrypt hash.
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// GenerateSignature generates a signature based on the provided questions and answers.
func GenerateSignature(username string, questions []string) string {
	// Combine the username with the questions for the signature
	combined := username + strings.Join(questions, "")

	hasher := sha256.New()
	hasher.Write([]byte(combined)) // Hash the combined string

	signature := hex.EncodeToString(hasher.Sum(nil)) // Convert the hash sum to a hex string

	return signature
}
