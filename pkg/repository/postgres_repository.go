// postgresRepository.go in /pkg/repository

package repository

import (
	"context"
	"database/sql"
	"fmt"
	"test_signer/pkg/models"
	"time"

	"github.com/lib/pq"
)

// PostgresRepository implements the IDBRepository interface for PostgreSQL.
type PostgresRepository struct {
	db *sql.DB
}

// NewPostgresRepository creates a new instance of PostgresRepository with a database connection.
func NewPostgresRepository(db *sql.DB) *PostgresRepository {
	return &PostgresRepository{db: db}
}

// CreateUser inserts a new user into the database and returns its ID.
func (r *PostgresRepository) CreateUser(ctx context.Context, user *models.User) (int, error) {
	query := `INSERT INTO users (username, password_hash) VALUES ($1, $2) RETURNING user_id`
	var userID int
	err := r.db.QueryRowContext(ctx, query, user.Username, user.PasswordHash).Scan(&userID)
	if err != nil {
		return 0, fmt.Errorf("CreateUser: %w", err)
	}
	return userID, nil
}

// CreateTest inserts a new test into the database and returns its ID.
func (r *PostgresRepository) CreateTest(ctx context.Context, test *models.Test) (int, error) {

	userQuery := `SELECT user_id FROM users WHERE username = $1`
	var userID int
	err := r.db.QueryRowContext(ctx, userQuery, test.Username).Scan(&userID)
	if err != nil {
		return 0, fmt.Errorf("failed to get userID: %w", err)
	}

	checkQuery := `
        SELECT EXISTS (
            SELECT 1 FROM tests
            WHERE user_id = $1 AND questions = $2
        )
    `
	var exists bool
	err = r.db.QueryRowContext(ctx, checkQuery, userID, pq.Array(test.Questions)).Scan(&exists)
	if err != nil {
		return 0, fmt.Errorf("error checking for existing test: %w", err)
	}
	if exists {
		return 0, fmt.Errorf("user has already submitted this test")
	}

	query := `
        INSERT INTO tests (user_id, questions, answers)
        VALUES ($1, $2, $3)
        RETURNING test_id
    `
	var testID int
	err = r.db.QueryRowContext(ctx, query, userID, pq.Array(test.Questions), pq.Array(test.Answers)).Scan(&testID)
	if err != nil {
		return 0, fmt.Errorf("CreateTest: %w", err)
	}

	return testID, nil
}

// CreateSignature inserts a new signature into the database and returns its ID.
func (r *PostgresRepository) CreateSignature(ctx context.Context, signature *models.Signature) (int, error) {
	query := `INSERT INTO signatures (test_id, signature) VALUES ($1, $2) RETURNING signature_id`
	var signatureID int

	err := r.db.QueryRowContext(ctx, query, signature.TestID, signature.Signature).Scan(&signatureID)
	if err != nil {
		return 0, fmt.Errorf("CreateSignature: %w", err)
	}
	return signatureID, nil
}

// GetSignatureByUserID retrieves the signature of the specified user from the database.
func (r *PostgresRepository) GetValidationByUsername(ctx context.Context, username, signature string) (bool, *time.Time, string, error) {
	var userID int
	userQuery := `SELECT user_id FROM users WHERE username = $1`
	err := r.db.QueryRowContext(ctx, userQuery, username).Scan(&userID)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil, "", fmt.Errorf("user not found")
		}
		return false, nil, "", fmt.Errorf("failed to get user ID: %w", err)
	}

	var exists bool
	var timestamp time.Time
	var answers string
	signatureQuery := `
	SELECT EXISTS(
		SELECT 1 FROM signatures WHERE test_id IN (
			SELECT test_id FROM tests WHERE user_id = $1
		) AND signature = $2
	), COALESCE(MAX(timestamp), 'epoch'), MAX(answers) FROM signatures
	INNER JOIN tests ON signatures.test_id = tests.test_id
	WHERE signatures.test_id IN (
		SELECT test_id FROM tests WHERE user_id = $1
	) AND signature = $2
	`
	err = r.db.QueryRowContext(ctx, signatureQuery, userID, signature).Scan(&exists, &timestamp, &answers)
	if err != nil {
		return false, nil, "", fmt.Errorf("failed to check signature existence and fetch answers: %w", err)
	}

	if timestamp.IsZero() {
		return exists, nil, "", nil
	}

	return exists, &timestamp, answers, nil
}
