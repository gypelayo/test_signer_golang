// test_repository.go in /pkg/repository

package repository

import (
	"context"
	"test_signer/pkg/models"
)

// TestRepository is responsible for handling test-related database operations.
type TestRepository struct {
	dbHandler IDBRepository
}

// NewTestRepository creates a new instance of TestRepository.
func NewTestRepository(dbHandler IDBRepository) *TestRepository {
	return &TestRepository{
		dbHandler: dbHandler,
	}
}

// CreateTest inserts a new test attempt into the database.
func (r *TestRepository) CreateTest(ctx context.Context, test *models.Test) (int, error) {
	return r.dbHandler.CreateTest(ctx, test)
}
