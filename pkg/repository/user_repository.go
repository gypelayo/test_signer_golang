// user_repository.go in /pkg/repository

package repository

import (
	"context"
	"test_signer/pkg/models"
)

// UserRepository is responsible for handling user-specific database operations.
type UserRepository struct {
	dbHandler IDBRepository
}

// NewUserRepository creates a new instance of UserRepository.
func NewUserRepository(dbHandler IDBRepository) *UserRepository {
	return &UserRepository{
		dbHandler: dbHandler,
	}
}

// CreateUser creates a new user in the database.
func (repo *UserRepository) CreateUser(ctx context.Context, user *models.User) (int, error) {
	return repo.dbHandler.CreateUser(ctx, user)
}
