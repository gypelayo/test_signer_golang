// db.go in /pkg/repository

package repository

import (
	"context"
	"test_signer/pkg/models"
	"time"
)

// IDBRepository defines the interface for database operations required by the Test Signer service.
// Implementations of this interface could interact with different kinds of databases (e.g., PostgreSQL, in-memory DB for testing).
type IDBRepository interface {
	CreateUser(ctx context.Context, user *models.User) (int, error)

	CreateTest(ctx context.Context, test *models.Test) (int, error)

	CreateSignature(ctx context.Context, signature *models.Signature) (int, error)

	GetValidationByUsername(ctx context.Context, username, signature string) (bool, *time.Time, string, error)
}
