package repository

import (
	"context"
	"test_signer/pkg/models"
	"time"
)

// SignatureRepository is responsible for handling signature-related database operations.
type SignatureRepository struct {
	dbHandler IDBRepository
}

// NewSignatureRepository creates a new instance of SignatureRepository.
func NewSignatureRepository(dbHandler IDBRepository) *SignatureRepository {
	return &SignatureRepository{
		dbHandler: dbHandler,
	}
}

// CreateSignature inserts a new signature into the database.
func (r *SignatureRepository) CreateSignature(ctx context.Context, signature *models.Signature) (int, error) {
	return r.dbHandler.CreateSignature(ctx, signature)
}

// GetSignatureByUserID retrieves the signature of the specified user from the database.
func (repo *SignatureRepository) GetValidationByUsername(ctx context.Context, username, signature string) (bool, *time.Time, string, error) {
	return repo.dbHandler.GetValidationByUsername(ctx, username, signature)
}
