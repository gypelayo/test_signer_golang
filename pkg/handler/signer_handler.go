package handler

import (
	"net/http"
	"test_signer/pkg/models"
	"test_signer/pkg/service"
	"test_signer/utils"

	"github.com/gin-gonic/gin"
)

// SignerHandler handles HTTP requests related to signing tests.
type SignerHandler struct {
	signerService *service.SignerService
}

// NewSignerHandler creates a new instance of SignerHandler.
func NewSignerHandler(signerService *service.SignerService) *SignerHandler {
	return &SignerHandler{
		signerService: signerService,
	}
}

// VerifySignatureRequest represents a request to verify a signature.
type VerifySignatureRequest struct {
	Username  string `json:"username"`
	Signature string `json:"signature"`
}

// VerifySignatureResponse represents the response for verifying a signature.
type VerifySignatureResponse struct {
	Message   string `json:"message"`
	Timestamp string `json:"timestamp"`
	Signature string `json:"signature"`
	Answers   string `json:"answers"`
}

// SignTestRequest represents a request to sign a test.
type SignTestRequest struct {
	JWT       string   `json:"jwt"`
	Username  string   `json:"username"`
	Questions []string `json:"questions"`
	Answers   []string `json:"answers"`
}

// SignTestResponse represents the response for signing a test.
type SignTestResponse struct {
	Message   string `json:"message"`
	Signature string `json:"signature"`
}

// CreateUserRequest represents a request to create a new user.
type CreateUserRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// CreateUserResponse represents the response for creating a new user.
type CreateUserResponse struct {
	Message string `json:"message"`
	Token   string `json:"token"`
}

// CreateUserHandler handles HTTP requests to create a new user and returns a token upon successful creation.
func (h *SignerHandler) CreateUserHandler(c *gin.Context) {

	var req CreateUserRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "failed to parse request body"})
		return
	}
	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "failed to hash password"})
		return
	}

	user := &models.User{
		Username:     req.Username,
		PasswordHash: hashedPassword,
	}

	token, err := h.signerService.CreateUser(c.Request.Context(), user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "failed to create user: " + err.Error()})
		return
	}

	resp := CreateUserResponse{Message: "User created successfully", Token: token}
	c.JSON(http.StatusOK, resp)
}

// SignTestHandler handles HTTP requests to sign a test.
func (h *SignerHandler) SignTestHandler(c *gin.Context) {

	var req SignTestRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "failed to parse request body"})
		return
	}

	signature, err := h.signerService.SignTest(c.Request.Context(), req.Username, req.JWT, req.Questions, req.Answers)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "failed to sign test: " + err.Error()})
		return
	}

	resp := SignTestResponse{Message: "Test signed successfully", Signature: signature}
	c.JSON(http.StatusOK, resp)
}

// VerifySignatureHandler handles HTTP requests to verify a signature.
func (h *SignerHandler) VerifySignatureHandler(c *gin.Context) {

	var req VerifySignatureRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "failed to parse request body"})
		return
	}
	timestamp, answers, err := h.signerService.VerifySignature(c.Request.Context(), req.Signature, req.Username)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "failed to verify signature: " + err.Error()})
		return
	}

	resp := VerifySignatureResponse{Message: "Signature verified successfully", Timestamp: timestamp.Format("2006-01-02 15:04:05"), Signature: req.Signature, Answers: answers}
	c.JSON(http.StatusOK, resp)
}
