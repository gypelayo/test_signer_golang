package models

type User struct {
	Username     string `db:"username"`
	PasswordHash string `db:"password_hash"`
}
