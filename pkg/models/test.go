package models

type Test struct {
	Username  string   `json:"username"`
	Questions []string `json:"questions"`
	Answers   []string `json:"answers"`
}
