package models

import "time"

type Signature struct {
	TestID    int       `json:"test_id"`
	Signature string    `json:"signature"`
	Timestamp time.Time `json:"timestamp"`
}
