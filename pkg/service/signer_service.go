// signer_service.go in /pkg/service

package service

import (
	"context"
	"errors"
	"fmt"
	"test_signer/pkg/models"
	"test_signer/pkg/repository"
	"test_signer/utils"
	"time"
)

// SignerService contains the business logic for the Test Signer service.
type SignerService struct {
	userRepository      repository.UserRepository
	testRepository      repository.TestRepository
	signatureRepository repository.SignatureRepository
}

// NewSignerService creates a new instance of SignerService.
func NewSignerService(userRepo repository.UserRepository, testRepo repository.TestRepository, signatureRepo repository.SignatureRepository) *SignerService {
	return &SignerService{
		userRepository:      userRepo,
		testRepository:      testRepo,
		signatureRepository: signatureRepo,
	}
}

// SignTest signs a test attempt for a given user.
func (s *SignerService) SignTest(ctx context.Context, username string, jwt string, questions []string, answers []string) (string, error) {

	err := utils.VerifyToken(jwt, username)
	if err != nil {
		return "", fmt.Errorf("failed to parse JWT token: %w", err)
	}

	test := &models.Test{
		Username:  username,
		Questions: questions,
		Answers:   answers,
	}

	var testID int

	testID, err = s.testRepository.CreateTest(ctx, test)
	if err != nil {
		return "", fmt.Errorf("failed to create test: %w", err)
	}

	signature := utils.GenerateSignature(username, questions)

	signatureObj := &models.Signature{
		TestID:    testID,
		Signature: signature,
		Timestamp: time.Now(),
	}

	_, err = s.signatureRepository.CreateSignature(ctx, signatureObj)
	if err != nil {
		return "", fmt.Errorf("failed to create signature: %w", err)
	}

	return signature, nil
}

// VerifySignature verifies if a signature belongs to the specified user.
func (s *SignerService) VerifySignature(ctx context.Context, signature, username string) (*time.Time, string, error) {

	validated, timestamp, answers, err := s.signatureRepository.GetValidationByUsername(ctx, username, signature)
	if err != nil {
		return nil, "", errors.New("signature verification failed" + err.Error())
	}

	if !validated {
		return nil, "", errors.New("signature verification failed")
	}

	return timestamp, answers, nil
}

// CreateUser creates a new user in the database and returns a token upon successful creation.
func (s *SignerService) CreateUser(ctx context.Context, user *models.User) (string, error) {

	_, err := s.userRepository.CreateUser(ctx, user)
	if err != nil {
		return "", fmt.Errorf("CreateUser: %w", err)
	}

	token, err := utils.CreateToken(user.Username)
	if err != nil {
		return "", fmt.Errorf("CreateUser: failed to generate token: %w", err)
	}

	return token, nil
}
