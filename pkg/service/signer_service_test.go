package service

import (
	"context"
	"fmt"
	"test_signer/pkg/models"
	"test_signer/pkg/repository"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// MockIDBRepository is a mock implementation of the IDBRepository interface for testing purposes.
type MockIDBRepository struct {
	CreateUserFunc              func(ctx context.Context, user *models.User) (int, error)
	CreateTestFunc              func(ctx context.Context, test *models.Test) (int, error)
	CreateSignatureFunc         func(ctx context.Context, signature *models.Signature) (int, error)
	GetValidationByUsernameFunc func(ctx context.Context, username, signature string) (bool, *time.Time, string, error)
}

func (m *MockIDBRepository) CreateUser(ctx context.Context, user *models.User) (int, error) {
	return m.CreateUserFunc(ctx, user)
}

func (m *MockIDBRepository) CreateTest(ctx context.Context, test *models.Test) (int, error) {
	return m.CreateTestFunc(ctx, test)
}

func (m *MockIDBRepository) CreateSignature(ctx context.Context, signature *models.Signature) (int, error) {
	return m.CreateSignatureFunc(ctx, signature)
}

func (m *MockIDBRepository) GetValidationByUsername(ctx context.Context, username, signature string) (bool, *time.Time, string, error) {
	return m.GetValidationByUsernameFunc(ctx, username, signature)
}

func TestSignTest_SuccessfulSignatureGeneration(t *testing.T) {
	mockRepo := &MockIDBRepository{
		CreateTestFunc: func(ctx context.Context, test *models.Test) (int, error) {
			return 1, nil // Simulate successful test creation
		},
		CreateSignatureFunc: func(ctx context.Context, signature *models.Signature) (int, error) {
			return 1, nil // Simulate successful signature creation
		},
	}

	// Initialize service with the mock repository
	signerService := NewSignerService(*repository.NewUserRepository(mockRepo), *repository.NewTestRepository(mockRepo), *repository.NewSignatureRepository(mockRepo))

	ctx := context.TODO()
	username := "user1"
	jwt := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDg4OTQ2MTUsInVzZXJuYW1lIjoidXNlcjEifQ.UyUfJIm-O-J_Vhb4R8n9VGiSpAc9Ylro0lHRlDSJgJI"
	questions := []string{"question1"}
	answers := []string{"answer1"}

	// Call SignTest
	signature, err := signerService.SignTest(ctx, username, jwt, questions, answers)

	// Assertions
	assert.NoError(t, err)
	assert.NotEmpty(t, signature)
}
func TestSignTest_ErrorCreatingTest(t *testing.T) {
	mockRepo := &MockIDBRepository{
		CreateTestFunc: func(ctx context.Context, test *models.Test) (int, error) {
			return 1, fmt.Errorf("error creating test") // Simulate successful test creation
		},
		CreateSignatureFunc: func(ctx context.Context, signature *models.Signature) (int, error) {
			return 1, nil // Simulate successful signature creation
		},
	}

	// Initialize service with the mock repository
	signerService := NewSignerService(*repository.NewUserRepository(mockRepo), *repository.NewTestRepository(mockRepo), *repository.NewSignatureRepository(mockRepo))

	ctx := context.TODO()
	username := "user12"
	jwt := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDg5NjI3NDAsInVzZXJuYW1lIjoidXNlcjEyIn0.WX18-8z0hSrM_qYkJqrQd614kTq6CYibKujEU8IjMlg"
	questions := []string{"question1"}
	answers := []string{"answer1"}

	// Call SignTest
	_, err := signerService.SignTest(ctx, username, jwt, questions, answers)
	// Assertions
	assert.Contains(t, err.Error(), "error creating test")
}

func TestSignTest_TokenVerificationFailure(t *testing.T) {
	mockRepo := &MockIDBRepository{
		CreateUserFunc: func(ctx context.Context, user *models.User) (int, error) {
			return 0, nil
		},
		CreateTestFunc: func(ctx context.Context, test *models.Test) (int, error) {
			return 0, nil
		},
		CreateSignatureFunc: func(ctx context.Context, signature *models.Signature) (int, error) {
			return 0, nil
		},
		// Simulate token verification failure
	}

	// Initialize service with the mock repository
	signerService := NewSignerService(*repository.NewUserRepository(mockRepo), *repository.NewTestRepository(mockRepo), *repository.NewSignatureRepository(mockRepo))

	_, err := signerService.SignTest(context.TODO(), "user1", "invalidJWT", []string{"question1"}, []string{"answer1"})

	// Assertions
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "failed to parse JWT token")
}
