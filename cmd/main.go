package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"test_signer/pkg/handler"
	"test_signer/pkg/repository"
	"test_signer/pkg/service"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

// Gin
func main() {

	dbConnection := os.Getenv("DB_CONNECTION")
	if dbConnection == "" {
		log.Fatalf("DB_CONNECTION environment variable is not set")
	}

	db, err := sql.Open("postgres", dbConnection)
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}
	defer db.Close()

	dbRepo := repository.NewPostgresRepository(db)
	userRepo := repository.NewUserRepository(dbRepo)
	testRepo := repository.NewTestRepository(dbRepo)
	signatureRepo := repository.NewSignatureRepository(dbRepo)

	signerService := service.NewSignerService(*userRepo, *testRepo, *signatureRepo)
	signerHandler := handler.NewSignerHandler(signerService)

	router := gin.Default()
	router.POST("/sign-test", signerHandler.SignTestHandler)
	router.POST("/verify-signature", signerHandler.VerifySignatureHandler)
	router.POST("/create-user", signerHandler.CreateUserHandler)

	appPort := os.Getenv("APP_PORT")
	if appPort == "" {
		appPort = "8075"
	}

	addr := fmt.Sprintf(":%s", appPort)
	log.Printf("Server listening on %s", addr)
	log.Fatal(http.ListenAndServe(addr, router))
}
